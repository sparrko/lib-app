import './MainTab.css';

function MainTab() {
  return (
    <div className="main-tab">
      <span className="title-in-tab">О создании сайта</span>
      <p>Данный сайт был разработан по тестовому заданию от Recode. Задание гласит что сайт должен использовать PHP веб-фреймворк Laravel.</p>
      <p>При создании использовались такие программы как:</p>
      <p>&nbsp;- Open Server;</p>
      <p>&nbsp;- Visual Code;</p>
      <p>&nbsp;- Microsoft Edge;</p>
      <p>&nbsp;- Blender.</p>
      <span className="title-in-tab">Исполнитель</span>
      <p>Демин Егор Олегович</p>
      <p>&nbsp;- <a href='https://vk.com/zateyliviynekto'>ВКонтакте</a>;</p>
      <p>&nbsp;- Почта Google: <a href='mailto:egordemin2890@gmail.com'>egordemin2890@gmail.com</a>;</p>
      <p>&nbsp;- Почта Yandex: <a href='mailto:roninblade11@yandex.ru'>roninblade11@yandex.ru</a>;</p>
      <p>&nbsp;- Номер телефона: 89372169430.</p>
      <p>&nbsp;</p>
    </div>
  );
}

export default MainTab;
